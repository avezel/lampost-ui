import { dispatch, dispatchBatch } from './dispatcher';

const req_map = {};

let current_req_id = 0;
let socket = null;
let connected = false;
let waitCount = 0;

const request = function request(path, data, withWait) {
  if (!connected) {
    dispatch('log', `Attempting request ${path} before socket connected`);
    return Promise.reject({id: 'Error', text: 'Socket not connected'});
  }
  if (withWait && waitCount++ == 0) {
    dispatch('remote_start_wait');
  }
  data = data || {};
  data.path = path;
  data.req_id = current_req_id++;

  return new Promise((resolve, reject) => {
    req_map[data.req_id] = {resolve, reject, withWait};
    socket.send(JSON.stringify(data));
  });
};

const send =  function send(path, data) {
  if (connected) {
    data = data || {};
    data.path = path;
    socket.send(JSON.stringify(data))
  } else {
    dispatch('log', `Attempt to send ${path} before socket connected`);
  }
};

const onMessage = function onMessage(message) {
  const response = JSON.parse(message.data);
  const req_id = response.req_id;
  if (req_id !== undefined) {
    const promise = req_map[req_id];
    if (!promise) {
      dispatch('log', `Received response for missing req_id ${req_id}`);
      return;
    }
    delete req_map[req_id];
    if (promise.withWait && --waitCount === 0) {
      dispatch('remote_stop_wait');
    }
    switch (response.http_status) {
      case 0:
      case 200:
      case 204:
      case undefined:
      case null:
        promise.resolve(response.data);
        break;
      case 401:
        dispatch('remote_request_denied');
        promise.reject(response);
        break;
      case 400:
      case 409:
        promise.reject(response);
        break;
      default:
        promise.reject(response);
        dispatch('log', `server_error:  ${response}`);
        break;
    }
  } else {
    dispatchBatch(response);
  }
};

const connect = function connect(options) {
  const { protocol, host} = window.location;
  const socket_url = `${protocol === "https:" ? "wss" : "ws"}://${host}/link`;
  socket = new WebSocket(socket_url);
  socket.onclose = (e) => {
    connected = false;
    dispatch('log', `Web socket close ${e}`);
    dispatch('remote_disconnect');
  };
  socket.onerror = (e) => {
    dispatch('log', `Web socket error ${e}`)
  };
  socket.onmessage = onMessage;
  socket.onopen = () => {
    connected = true;
    send('session_connect', options);
  };
};

export {
  connect,
  send,
  request
};

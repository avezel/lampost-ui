import fontawesome from '@fortawesome/fontawesome';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import circleArrowRight from '@fortawesome/fontawesome-free-solid/faArrowCircleRight';

fontawesome.library.add(circleArrowRight);

export default FontAwesomeIcon;
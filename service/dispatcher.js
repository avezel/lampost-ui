const registry = {};

const register = function register(event_type, callback, priority = 0) {
  const regs = registry[event_type] = registry[event_type] || [];
  const registration = { event_type, callback, priority };
  regs.push(registration);
  regs.sort(function (a, b) {
    return a.priority - b.priority;
  });
  return registration;
};

const unregister = function unregister(registration) {
  const registrations = registry[registration.event_type];
  if (!registrations) {
    dispatch('log', `Unregistering event for ${registration.event_type} that was never registered`);
    return;
  }
  let i;
  for (i = 0; i < registrations.length; i++) {
    if (registrations[i] === registration) {
      registrations.splice(i, 1);
      if (!registrations.length) {
        delete registry[registration.event_type];
      }
      return;
    }
  }
  dispatch('log', `Failed to unregister event ${registration.event_type}`);
};

const dispatch = function dispatch() {
  const event_type = arguments[0];
  let i;
  const args = [];
  for (i = 1; i < arguments.length; i++) {
    args.push(arguments[i]);
  }
  const registrations = registry[event_type];
  if (registrations) {
    for (i = 0; i < registrations.length; i++) {
      registrations[i].callback.apply(this, args);
    }
  }
};

const dispatchMap  = function dispatchMap(eventMap) {
  for (let key in eventMap) {
    if (eventMap.hasOwnProperty(key)) {
      dispatch(key, eventMap[key]);
    }
  }
};

const dispatchBatch = function dispatchBatch(batch) {
  if (Array.isArray(batch)) {
    batch.forEach(dispatchMap);
  } else {
    dispatchMap(batch);
  }
};

class Listener {
  registrations = [];
  register(event_type, callback, priority = 0) {
    this.registrations.push(register(event_type, callback, priority));
  }
  destroy() {
    this.registrations.forEach((reg) => unregister(reg));
  }
}

setInterval(() => dispatch('heartbeat'), 30000);

export {
  register,
  unregister,
  dispatch,
  dispatchBatch,
  Listener
};

import { Label as RebassLabel, Button as RebassButton, Text as RebassText, BlockLink as RebassBlockLink } from 'rebass';

const Label = RebassLabel.extend`
  font-weight: 700;
`;

const Button = RebassButton.extend`
  cursor: pointer;
`;

const Bold = RebassText.extend`
  font-weight: bold;
`;

const BlockLink = RebassBlockLink.extend`
  cursor: pointer;
`;

export { Label, Button, Bold, BlockLink };
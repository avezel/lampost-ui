import system from 'system-components';
import styled from 'styled-components';

const TableRow = system({
  width: '100%',
  display: 'flex',
  flexDirection: 'row'
}, [], {
  borderBottom: '1px solid #e0e0e0'
});

const HeaderRow = styled(TableRow)([], (props) => ({
  fontWeight: 700,
  backgroundColor: '#FFEEDB'
}));

const Header = system({
  px: 1,
  py: [1, 1, 2]
}, ['width', 'fontSize'], {
  flexGrow: 0,
  flexShrink: 0,
  lineHeight: '1.2em'
});

const Cell = system({
  p: [1, 1, 1, 2],
  bg: 'white'
}, ['width'], {
  flexGrow: 0,
  flexShrink: 0,
  lineHeight: '1.2em'
});

export { Cell, TableRow, HeaderRow, Header };
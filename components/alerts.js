import React from 'react';

import { Text } from 'rebass';

const ErrorText = Text.extend`
  color: red;
  font-style: oblique;
`;

const SuccessText = Text.extend`
  color: green;
  font-style: oblique;
`;

export { ErrorText, SuccessText };

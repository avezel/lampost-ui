import { observable, action, computed, flow } from 'mobx';

import { dispatch, Listener } from '../service/dispatcher';
import { request, send } from '../service/remote';
import { stringSort } from '../util/misc'

class EditStore extends Listener {

  @observable adminOps = [];
  @observable editError = '';
  @observable operationResult = null;

  loadAdminOps = flow(function * () {
    try {
      const ops = yield request('admin/operations', {name: 'list'});
      this.adminOps = ops.sort(stringSort('name'));
      this.adminOps.forEach(op => op.paramValues = [].fill('', 0, op.params.length));
    } catch(e) {
      this.editError = e.client_message ? e.client_message : e.toString();
    }
  });

  executeOp = flow(function * (op) {
    try {
      this.operationResult = yield request('admin/operations', op);
    } catch (e) {
      this.operationResult = e.client_message ? e.client_message : e.toString();
    }
  });
}

export default new EditStore();
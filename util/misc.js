const storageAvailable = function storageAvailable(type) {
    try {
        var storage = window[type],
            x = '__storage_test__';
        storage.setItem(x, x);
        storage.removeItem(x);
        return true;
    }
    catch(e) {
        return false;
    }
};

const stringSort = function stringSort(field) {
  return (a, b) => {
    const aVal = (a[field] || '').toLocaleLowerCase();
    const bVal = (b[field] || '').toLocaleLowerCase();
    return aVal.localeCompare(bVal);
  };
};

const alphaSort = function alphaSort(a, b) {
  const aVal = (a || '').toLocaleLowerCase();
  const bVal = (b || '').toLocaleLowerCase();
  return aVal.localeCompare(bVal);
};

const numericSort = function numericSort(field) {
  return (a, b) => {
    const aVal = (a[field] || 0);
    const bVal = (b[field] || 0);
    return aVal - bVal;
  }
};

const clearObj = function clearObj(obj) {
  for (let key in obj) {
    if (obj.hasOwnProperty(key) && typeof obj[key] !== 'function') {
      delete obj[key];
    }
  }
};

export {
  storageAvailable,
  stringSort,
  alphaSort,
  numericSort,
  clearObj
}

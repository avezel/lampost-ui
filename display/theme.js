export default {
  colors: {
    black: '#000',
    white: '#FFF',
    primary: '#337ab7',
    blue: '#337ab7',
    warning: '#f0ad4e',
    danger: '#d9534f'
  },
  fonts: {
    sans: '"Helvetica Neue", Helvetica, Arial, sans-serif'
  }
}
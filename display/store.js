import { observable, action, computed, autorun } from 'mobx';

import { storageAvailable } from '../util/misc';
import { Listener } from '../service/dispatcher';
import { send } from '../service/remote';

const canStore = storageAvailable('localStorage') && storageAvailable('sessionStorage');
const sessionKey = 'recentSessions';
const lastPlayerKey = 'lastPlayer';
const staleInterval = 5 * 60 * 1000; // Forget sessions after five minutes

class DisplayStore extends Listener {

  @observable backgroundImage = null;
  @observable page = null;
  @observable playerId = null;
  @observable display = [];
  @observable connectState = 'connecting';
  @observable loginState = null;
  @observable clientConfig = {};
  @observable systemMessages = [];
  @observable heartbeat = Date.now();
  @observable disableNav = false;
  @observable defaultPage = null;

  pages = new Map();
  validPages = new Set();
  sessionId = null;
  player = null;

  constructor() {
    super();
    this.register('connect', action(({session_id, connect_type}) => {
      this.connectState = 'connected';
      this.sessionId = session_id;
      if (connect_type === 'new' && this.player) {
        if (!this.reLogin()) {
          this.systemMessages.push('Unable to Reconnect to Previous Session');
          this.logout();
        }
      }
    }));

    this.register('remote_disconnect', action(() => {
      this.connectState = 'reconnecting';
    }));

    this.register('navigate', this.updatePath);

    this.register('disable_nav', action(() => this.disableNav = true));

    this.register('enable_nav', action(() => this.disableNav = false));

    if (process.env.NODE_ENV === 'dev') {
      this.register('login_failure', () => {
        if (this.player) {
          this.logout();
        }
      });
    }

    this.register('login', action((data) => {
      this.loginState = null;
      for (let key in data) {
        if (data.hasOwnProperty(key) && this.hasOwnProperty(key)) {
          this[key] = data[key];
        }
      }
      this.playerId = data.player.dbo_id;
      this.updateStoredSessions();
    }));

    this.register('logout', action(this.logout));

    this.register('client_config', action((data) => {
       this.clientConfig = data;
    }));

    this.register('heartbeat', action(() => {
      this.heartbeat = Date.now();
      if (this.playerId) {
        this.updateStoredSessions();
      }
    }));
  }

  @computed get connectOptions() {
    return { player_id: this.playerId, session_id: this.sessionId };
  };

  @computed get validPage() {
    return this.page && this.validPages.has(this.page.path);
  }

  @action reLogin() {
    if (process.env.NODE_ENV === 'dev') {
      const lastLogin = sessionStorage.getItem('login');
      if (lastLogin) {
        this.loginState = 'loggingIn';
        send("player_login", JSON.parse(lastLogin));
        return true;
      }
    }
    return false;
  }

  @action updatePath = (value) => {
    if (!this.disableNav) {
      this.page = this.pages.get(value);
    }
  };

  logout = () => {
    const oldPlayer = this.playerId;
    this.player = null;
    this.playerId = null;
    this.loginState = null;
    this.updateStoredSessions(oldPlayer);
  };

  updateStoredSessions = (oldPlayerId) => {
    if (!canStore) {
      return;
    }
    const { sessionId, playerId, heartbeat } = this;
    const stored = localStorage.getItem(sessionKey);
    const storedSessions = stored ? new Map(JSON.parse(stored)) : new Map();
    if (playerId) {
      sessionStorage.setItem(lastPlayerKey, playerId);
      storedSessions.set(playerId, { player_id: playerId, session_id: sessionId, heartbeat });
    } else {
      sessionStorage.removeItem(lastPlayerKey);
      sessionStorage.removeItem('login');
    }
    if (oldPlayerId) {
      const oldSession = storedSessions.get(oldPlayerId);
      if (oldSession && oldSession.session_id === sessionId) {
        storedSessions.delete(oldPlayerId);
      }
    }
    localStorage.setItem(sessionKey, JSON.stringify([...storedSessions]));
  };

  addPage = (path, component) => {
    this.pages.set(path, {path, component});
    if (!this.defaultPage) {
      this.defaultPage = this.pages.get(path);
    }
  };

  enablePage = (path) => {
    this.validPages.add(path);
  };

  disablePage = (path) => {
    this.validPages.delete(path);
  };

  lastSession = () => {
    if (!canStore) {
      return null;
    }

    const stored = localStorage.getItem(sessionKey);
    const storedSessions = stored ? new Map(JSON.parse(stored)) : new Map();

    const staleTime = Date.now() - staleInterval;
    for (let session of storedSessions.values()) {
      if (session.heartbeat < staleTime) {
        storedSessions.delete(session.player_id);
      }
    }
    localStorage.setItem(sessionKey, JSON.stringify([...storedSessions]));
    let lastSession = storedSessions.get(sessionStorage.getItem(lastPlayerKey));
    if (!lastSession) {
      for (let session of storedSessions.values()) {
        if (!lastSession || lastSession.heartbeat < session.heartbeat) {
          lastSession = session;
        }
      }
    }
    return lastSession;
  }
}

const store = new DisplayStore();


export default store;

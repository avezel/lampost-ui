import React from 'react';
import ReactDOM from 'react-dom';
import createHistory from 'history/createBrowserHistory';

import { autorun } from 'mobx';

import store from './store';
import Reconnect from '../pages/reconnect';
import Login from '../pages/login';

const history = createHistory();

const providers = [];

const reactRender = function reactRender(component) {

  let current = React.createElement(component);
  for (let provider of providers) {
    current = React.createElement(provider._provider || provider, provider._props || {}, current);
  }
  ReactDOM.render(
    current,
    document.getElementById("app")
  );
};

const render = function render() {

  if (store.connectState === 'connecting' || store.loginState === 'loggingIn') {
    return;
  }

  if (store.connectState === 'reconnecting') {
    reactRender(Reconnect);
    return;
  }

  if (store.playerId === null) {
    reactRender(Login);
    return;
  }

  if (store.validPage) {
    const { path, component } = store.page;
    if (history.location.pathname != path) {
      history.push(path);
    }
    reactRender(component);
    return;
  }

  reactRender(store.defaultPage.component);

};

const updatePath = function updatePath() {
  const path = history.location.pathname;
  if (store.pages.has(path)) {
    store.updatePath(path);
    if (store.validPage) {
      if (store.page.path !== history.location.pathname || history.location.hash || history.location.search) {
        history.replace(store.page.path);
      }
      return;
    }
  }
  history.replace(store.defaultPage.path);
};

const start = function start(opt_providers) {
  providers.push(...opt_providers);
  updatePath();
  history.listen(updatePath);
  autorun( render );
};


export default {
  start,
  updatePath
};



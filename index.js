import React from 'react';
import { Provider as RebassProvider}  from 'rebass';

import { connect } from './service/remote';
import { Listener, register } from './service/dispatcher';

import store from './display/store';
import nav from './display/nav';
import defaultTheme from './display/theme';

import './service/icons.js'

const bootstrap = function bootstrap({providers = [], theme = null} = {}) {
  if (console) {
    register('log', (msg) => console.log(msg));
  }

  let start = new Listener();

  const firstRender = function firstRender() {
    start.destroy();
    start = null;
    const rebassProvider = {_provider: RebassProvider, _props: { theme: theme || defaultTheme }};
    nav.start([rebassProvider].concat(providers));
  };

  start.register('connect', firstRender);
  start.register('remote_disconnect', firstRender);

  connect(store.lastSession() || {});
};

export default bootstrap;
import React from 'react';
import { observable, action } from 'mobx';
import { observer } from 'mobx-react';

import { send, request } from '../service/remote';
import { register, dispatch, Listener } from '../service/dispatcher';
import { ErrorText } from '../components/alerts';
import { Label, Button } from '../components/forms';
import displayStore from '../display/store';

import { Box, Flex, Heading, Text, Input, Row, Column } from 'rebass';

class LoginStore extends Listener {
  @observable user_name = '';
  @observable password = '';
  @observable view = 'login';
  @observable login_failure = '';
  @observable email = '';
  @observable password_copy = '';

  constructor() {
    super();
    this.register('login_failure', action ((failure) => this.login_failure = failure));
  }

  @action updateUser = (e) => {
    this.clearError();
    this.user_name = e.target.value;
  };

  @action updatePassword = (e) => {
    this.clearError();
    this.password = e.target.value;
  };

  @action updateCopy = (e) => {
    this.clearError();
    this.password_copy = e.target.value;
  };

  @action updateEmail = (e) => {
    this.clearError();
    this.email = e.target.value;
  };

  @action clearAccount = (e) => {
    e.preventDefault();
    this.clearError();
    this.email = null;
    this.passwordCopy = null;
    this.password = null;
    this.user_name = null;
    this.view = 'login';
  };

  @action clearError = () => this.login_failure = '';

  @action submitLogin = (e) => {
    e.preventDefault();
    e.stopPropagation();
    const { user_name, password } = this;
    if (user_name && password) {
      if (process.env.NODE_ENV === 'dev') {
        sessionStorage.setItem('login', JSON.stringify({user_name, password}));
      }
      send("player_login", {user_name, password});
    } else {
      this.login_failure = "User name and password required.";
    }
  };

  @action saveLogin = (data) => {
    console.log("ENV:: " + process.env.NODE_ENV);

  };

  @action submitAccount = (e) => {
    e.preventDefault();
    if (this.password != this.password_copy) {
      this.login_failure = "Passwords don't match.";
      return;
    }
    if (this.user_name.indexOf(" ") > -1) {
      this.login_failure = "Spaces not permitted in account names";
      return;
    }
    request('settings/create_account', {account_name: this.user_name, password: this.password, email: this.email})
      .then((data) => {
        dispatch('user_created', data)
      })
      .catch((exp) => {
        if (exp.http_status === 409) {
          this.login_failure = "That user name is already in use";
        } else {
          this.login_failure = exp.client_message;
        }
      })
  };
}

@observer
class LoginForm extends React.Component {
  render() {
    const store = this.props.store;

    return (
      <React.Fragment>
        <Row p={3} fontSize={[2, 3]}>
          <Text>Please Log In or</Text>
          <Button ml={3} onClick={() => store.view = 'account'}>Create An Account</Button>
        </Row>
        <form onSubmit={store.submitLogin}>
          <Row>
            <Column p={3} width={2/3} my={2}>
               <Label htmlFor="login_player">Player or Account Name</Label>
               <Input mb={2} autoFocus id="login_player" onChange={store.updateUser} required />
               <Label htmlFor="login_password">Password</Label>
               <Input mb={2} id="login_password" type="password" onChange={store.updatePassword} required />
            </Column>
            <Box p={3} width={1/3} mt={4} >
              <Button mb={2} type="submit">Enter {displayStore.clientConfig.lampost_title}</Button>
              <ErrorText>{store.login_failure} </ErrorText>
            </Box>
          </Row>
        </form>
      </React.Fragment>
    )
  }
}

@observer
class AccountForm extends React.Component {
  render() {
    const store = this.props.store;
    return (
      <form onSubmit={store.submitAccount}>
        <Row mt={2}>
          <Column>
            <Label htmlFor="login_player">Account Name</Label>
            <Input autoFocus id="login_player" type="text" onChange={store.updateUser} />
            <Label htmlFor="email">Email</Label>
            <Input id="email" type="text" onChange={store.updateEmail} />
            </Column>
          <Column>
            <Label htmlFor="login_password">Password</Label>
            <Input id="login_password" type="password" onChange={store.updatePassword} />
            <Label htmlFor="password_copy">Retype Password</Label>
            <Input id="password_copy" type="password" onChange={store.updateCopy} />
          </Column>
        </Row>
        <Row justify='center'>
          <Button mr={3} type="submit" children='Create Account' />
          <Button onClick={store.clearAccount} children='Cancel' />
        </Row>
        <Row justify='center' mt={2}>
          <ErrorText>{store.login_failure} </ErrorText>
        </Row>
      </form>
    )
  }
}

@observer
class Login extends React.Component {
  store = new LoginStore();

  componentWillUnmount() {
    this.store.destroy();
  }

  render() {
    const sectionStyle = {};
    if (displayStore.backgroundImage) {
      sectionStyle.backgroundImage = `url('${displayStore.backgroundImage}')`;
      sectionStyle.minHeight =  window.innerHeight / 2;
    }
    return (
      <React.Fragment>
        <Box id="fred" color="white" p={3} style={sectionStyle} >
          <Heading pb={2} fontSize={7}>Welcome to {displayStore.clientConfig.lampost_title}</Heading>
          <Heading fontSize={4} dangerouslySetInnerHTML={{__html: displayStore.clientConfig.lampost_description}} />
        </Box>
        <Flex align='center' justify='center'>
          <Box  width={[1, 3/4, 2/3, 1/2, 1/3]}>
            {this.store.view === 'login' && <LoginForm store={this.store} />}
            {this.store.view === 'account' && <AccountForm store={this.store} />}
          </Box>
        </Flex>
    </React.Fragment>)
  }
}

export default Login;

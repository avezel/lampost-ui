import React from 'react';
import { observable, action, computed } from 'mobx';
import { observer } from 'mobx-react';

import { Flex, Row, Button, Checkbox, Input } from 'rebass';

import { request } from '../service/remote';
import { ErrorText, SuccessText } from '../components/alerts';
import { Label } from '../components/forms';

const file_name = observable.box('');
const clear_existing = observable.box(false);
const success_message = observable.box(null);
const error_message = observable.box(null);

const clearMessages = action(() => {
  success_message.set(null);
  error_message.set(null);
});

const updateFileName = action((e) => {
  clearMessages();
  file_name.set(e.target.value);
});

const updateClear = action((e) => {
  clearMessages();
  clear_existing.set(e.target.checked);
});

const submitImport = action((e) => {
  e.preventDefault();
  clearMessages();
  request('admin/yaml_import', {file_name, clear_existing})
    .then(() => success_message.set('Import Successful'))
    .catch((e) => {
      console.log('ERROR');
      console.log(e);
      error_message.set(`Status: ${e.http_status}  Message: ${e.client_message} `)
    }
  )
});

@observer
class ImportPage extends React.Component {

  componentWillUnmount() {
    clearMessages();
  }

  render() {
    return (
      <form onSubmit={submitImport}>
        <Flex flexDirection='column' width={[1, 3/5, 1/2, 1/3]} m={4}>
          <Label htmlFor="file_name">Enter Data File Name</Label>
          <Input autoFocus id="file_name" type="text" onChange={updateFileName} />
          <Label mt={2}>
            <Checkbox id="clear_existing" onChange={updateClear}/>
                &nbsp;Clear Existing Data
          </Label>
        </Flex>
        <Row mx={4}>
          <Button type="submit" mr={2}>Begin Import</Button>
          {success_message.get() && <SuccessText>{success_message.get()}</SuccessText>}
          <ErrorText>{error_message.get()}</ErrorText>
        </Row>
      </form>
    )
  }
}

export default ImportPage;

import React from 'react';
import { connect } from '../service/remote';

class Reconnect extends React.Component {
  state = { time: 15 };
  timer = null;

  componentWillMount = () => {
    this.timer = setInterval(this.countdown, 1000);
  };

  componentWillUnmount = () => {
    clearInterval(this.timer);
  };

  countdown = (force) => {
    const newTime = (force || this.state.time === 1) ? 15 : this.state.time - 1;
    if (newTime === 15) {
      connect(this.props.connectOptions);
    }
    this.setState({time: newTime});
  };

  render() {
    return (<div className="fullscreen">
      <h3 className="system_msg">Server Connection Lost</h3>
      <div className="loader"></div>
      <h3>Reconnecting in {this.state.time} seconds</h3>
      <button className="button is-primary"
              onClick={() => this.countdown(true)}>Reconnect Now</button>
    </div>)
  }
}

export default Reconnect


import React from 'react';
import { observable, action, computed } from 'mobx';
import { observer } from 'mobx-react';
import { Flex, Text, Box, Input, Row, Column} from 'rebass';

import editStore from 'lampost-ui/editor/editstore';
import { ErrorText, SuccessText } from 'lampost-ui/components/alerts';
import { Button } from 'lampost-ui/components/forms';
import { TableRow, HeaderRow, Header, Cell } from 'lampost-ui/components/table';

@observer
class OpRow extends React.Component {

  render() {

    const { name, args, params } = this.props.op;

    const empty = (4 - args.length) * (1/6);

    return (<TableRow>
      <Cell bg={'#eee'} width={1/4}>{name}</Cell>
      {args.map((arg, ix) => <Cell width={1/6} key={arg}>{arg}<Input onChange={(e) => params[ix] = e.target.value} value={params[ix]} /></Cell>)}
      {empty && <Cell width={empty} />}
      <Cell width={1/12}><Button children='Execute' onClick={() => editStore.executeOp(this.props.op)}/></Cell>
    </TableRow>)
  }
}

@observer
class AdminGrid extends React.Component {

  componentWillMount() {
    editStore.loadAdminOps();
  }

  render() {
    return (
      <Flex flexDirection='column'>
        <HeaderRow>
          <Header width={1/4}>Admin Operations</Header>
        </HeaderRow>
        {editStore.adminOps.map((op) => <OpRow key={op.name} op={op} />)}
        <ErrorText>{editStore.editError}</ErrorText>
        {editStore.operationResult && (<Flex ml={3}>
          <SuccessText mr={2}>{editStore.operationResult}</SuccessText>
          <Button onClick={() => editStore.operationResult = null}>Clear</Button>
        </Flex>)}
      </Flex>
    )
  }
}

export default AdminGrid;